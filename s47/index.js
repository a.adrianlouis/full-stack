//The 'document' represent the whole HTML page, and the query selector targets a specific element based on its ID, class, or tag name

console.log(document.querySelector("#txt-first-name"))
console.log(document.querySelector("#txt-last-name"))

//These are the specific selectors you can use for targeting a specific target method (ID, class, tag nmame)
//Note: These arent commont to use anymore as compared to "querySelector"
// document.getElementById("txt-first-name")
// document.getElementByClassName("span-full-name")
// document.getElementByTagName("input")


// [SECTION] Event Listeners
const txt_first_name = document.querySelector("#txt-first-name")
let txt_last_name = document.querySelector("#txt-last-name")
let span_full_name = document.querySelector(".span-full-name")

//The 'addEventListener' function listens for an event in a specific HTML tag. Antime an event is triggered, it will run the function in its 2nd argument
txt_first_name.addEventListener('keyup', (event) => {
	span_full_name.innerHTML = txt_first_name.value
})

txt_last_name.addEventListener('keyup' , (event) => {
	span_full_name.innerHTML = txt_first_name.value + " " + txt_last_name.value
})


// The 'event' object represents the actual event in the HTML element (keyup) and you can use that object's 'target' property to access the HTML element itself
txt_first_name.addEventListener('keyup', (event) => {
	console.log(event.target)
	console.log(event.target.value)
})

